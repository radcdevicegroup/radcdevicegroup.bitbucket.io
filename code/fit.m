function [model, exit] = fit(model, features, labels, eta)
%FIT a model to labels
%   MODEL = FIT(MODEL, FEATURES, LABELS, ETA) find a vector of weights to
%   minimize the error of MODEL. MODEL should be a function that accepts a
%   vector of WEIGHTS and returns a prediction. FEATURES should be a matrix of
%   feature vectors for different observations and LABELS should be the true
%   class for each observation. MODEL's prediction using the current iterations
%   weights is compared to the true LABELS to determine its error. The rate
%   weights are changed is determined by the error using the current weights and
%   the learning rate ETA.
%
%   The MODEL is iteratively fit to the labels until the mean squared
%   error is small, the sum weight change is small, or the max number of
%   iterations is reached.
%
%   The returned MODEL is the same MODEL input with the weights set to the
%   trained weights such that the returned MODEL is called MODEL(FEATURES)
%   instead of MODEL(FEATURES, WEIGHTS).
%
%   [MODEL, EXIT] = FIT(MODEL, FEATURES, LABELS, ETA) return the exit
%   condition that lead to the training terminating. EXIT of 0 means error
%   was below a small level, 1 means hit max number of iterations, and 2
%   means weight change was small.

    nFeatures = size(features, 2);
    weights = randn(nFeatures, 1); % Initial guess.
    delta = ones(nFeatures, 1);    % Change in weights.
    mse = @(weights) mean((model(features, weights) - labels) .^ 2);

    H = 0.25;
    EPSILON = 10 ^ -5;
    MAXITER = 10000;

    iter = 0;
    while (mse(weights) > EPSILON) && (iter < MAXITER) && (sum(delta .^ 2) > EPSILON)
        iter = iter + 1;

        for i = 1:nFeatures
            delta(i) = derivative(i);
        end
        weights = weights - (eta * delta);
    end
    model = @(features) model(features, weights);

    if mse(weights) <= EPSILON
        exit = 0;
    elseif iter == MAXITER
        exit = 1;
    elseif sum(delta .^ 2) <= EPSILON
        exit = 2;
    else
        exit = 3;
    end

    function delta = derivative(i)
        left = weights; right = weights;
        left(i) = left(i) - H;
        right(i) = right(i) + H;
        delta = (mse(right) - mse(left)) / (2 * H);
    end
end
