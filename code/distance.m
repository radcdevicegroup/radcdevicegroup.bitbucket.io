function d = distance(t)
%DISTANCE calculate the euclidean distance between NCI and the other states

    states = findgroups(t.dcfdx);
    means = [splitapply(@mean, t.tactivity_acth_avg, states), ...
             splitapply(@mean, t.pert_noact_avg, states)];
    vars = [splitapply(@var, t.tactivity_acth_avg, states), ...
            splitapply(@var, t.pert_noact_avg, states)];
    n = splitapply(@length, t.dcfdx, states);
    sp = pooledVar(vars, n);

    d = sqrt(sum(((means(1, :) - means) .^ 2) ./ sp, 2));
    d = d(2:end);

    function sp = pooledVar(v, n)
        sp = sum(v .* (n - 1), 1) ./ sum(n);
    end
end
