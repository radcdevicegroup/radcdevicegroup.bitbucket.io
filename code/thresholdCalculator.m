function thresholdCalculator(record)
    import pipeline.actical.avgActivityPerActiveHour
    import pipeline.actical.avgDailyInactivity

    thresholds = 0:100:1000;
    counts = record.getSignal('counts');
    for thresh = thresholds
        variableSet = ['t' num2str(thresh)];
        addVariable = @(varname, value) record.addVariable(variableSet, varname, value);

        addVariable('tactivity_acth_avg', ...
                    avgActivityPerActiveHour(counts, thresh));

        addVariable('pert_noact_avg', ...
                    avgDailyInactivity(counts, thresh));
    end
end
