function indices = labelGui(record)
%LABELGUI determine days
%   INDICES = LABELGUI(RECORD) display the RECORD's accelerometer signal in a
%   plot with check boxes to select days as good (checked) or bad
%   (unchecked). Return an indices vector with the resulting check box states.

    acc = record.getSignal('accelerometer');
    days = acc.getNumDays;

    % Downsample to reduce memory requirements.
    acc.slice(1:acc.fs*15:length(acc));
    x = linspace(0, days, length(acc));
    plot(x, acc.data)
    xlabel('Day')
    ylabel('Acceleration (g)')

    set(gca, 'XGrid', true)
    set(gca, 'XTick', 0:(days - 1));
    set(gca, 'XLim', [0 days]);
    set(gca, 'YLim', [-2 2]);

    next = uicontrol;
    next.Parent = gcf;
    next.Style = 'pushbutton';
    next.Units = 'Normalized';
    next.Position = [0.9, 0.005, 0.09, 0.08];
    next.String = 'Continue';
    next.Callback = {@setIndicesAndClose, days};

    axPos = get(gca, 'Position');
    boxes = cell(days, 1);
    indices = true(days, 1);
    for d = 1:days
        boxes{d} = addCheckBox(d);
    end
    uiwait

    function box = addCheckBox(day)
        width = 0.025;
        height = 0.05;
        axWidth = axPos(3);
        dayWidth = axWidth / days;
        relXPos = ((day - 0.5) * dayWidth) - (width / 4);

        box = uicontrol;
        box.Parent = gcf;
        box.Style = 'checkbox';
        box.Units = 'Normalized';
        box.Position = [relXPos + axPos(1),
                        axPos(2) - (1.25 * height),
                        width,
                        height];
        box.Value = true;
    end

    function setIndicesAndClose(~, ~, days)
        for d = 1:days
            indices(d) = boxes{d}.Value;
        end
        close(gcf);
    end
end
