function guiDayLabeler(record)
%GUIDAYLABELER gui pipeline calculator to mark days as good or bad
%   GUIDAYLABELER(RECORD) a pipeline calculator to manually label days in an
%   accelerometer signal as good or bad based on whether appearance.

    indices = labelGui(record);

    addVariable = @(name, value) record.addVariable('label', name, value);
    addVariable('day', 1:record.getNumDays);
    addVariable('isgood', indices);
end
