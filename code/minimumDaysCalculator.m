function minimumDaysCalculator(record)
%MINIMUMDAYSCALCULATOR calculate variables using cumulative number of days

    MINDAYS = 2;          % Minimum number of days to use in calculation.
    MAXDAYS = 7;          % Maximum number of days to use in calculation.
    HOTSETSTART = 2;      % Day to start first set on.
    COOLEDSETSTART = 5;   % Day to start second set on.
    DAYSUNTILSTABLE = 10; % Days needed to get stable results.

    if record.getNumDays < DAYSUNTILSTABLE
        return
    end

    selectDays = generateDaySelector(record);
    cumCalcVariables(DAYSUNTILSTABLE, DAYSUNTILSTABLE, DAYSUNTILSTABLE, 'goldStandard');
    cumCalcVariables(HOTSETSTART, MINDAYS, MAXDAYS , 'hotSet');
    cumCalcVariables(COOLEDSETSTART, MINDAYS, MAXDAYS, 'cooledSet');

    function cumCalcVariables(start, minDays, maxDays, setName)
        firstDay = start - minDays + 1;
        lastDay = firstDay + maxDays - 1;
        numDaysUsed = minDays:maxDays;
        for today = start:lastDay
            selectDays(firstDay:today)
            pipeline.actical.calculateAll(record);
            moveVariableSet('actical', setName);
        end
        record.addVariable(setName, 'numDaysUsed', numDaysUsed);
        record.removeVariableSet('actical');

        function moveVariableSet(current, dest)
            isdestnew = ~contains(dest, record.listVariableSets);
            if isdestnew
                empty = zeros(length(numDaysUsed), 1);
                for f = record.listVariables(current)'
                    record.addVariable(dest, f{1}, empty);
                end
            end

            for f = record.listVariables(current)'
                vars = record.variables.(dest).(f{1});
                record.removeVariable(dest, f{1});
                vars(today - start + 1) = record.variables.(current).(f{1});
                record.addVariable(dest, f{1}, vars);
            end
        end
    end
end

function selector = generateDaySelector(record)
    masterCounts = record.getSignal('counts');
    function acticalRecrod = selectDays(desiredDays)
        record.removeSignal('counts');
        record.addSignal('counts', masterCounts.copy);
        index = false(1, record.getNumDays);
        index(desiredDays) = true;
        record.setDaysWorn(index);
    end

    selector = @selectDays;
end
